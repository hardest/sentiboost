/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TypedDependency;
import org.apache.commons.collections.IteratorUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test case for {@link org.fbk.sentiboost.StanfordParser}.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class StanfordParserTest {

    protected static final String TARGET_TEXT = "Mortar assault leaves at least 18 dead.";

    private StanfordParser parser;

    @Before
    public void setUp() {
        parser = new StanfordParser();
    }

    @Test
    public void testTokenize() throws IOException {
        final DocumentPreprocessor documentPreprocessor = parser.tokenize(TARGET_TEXT);
        for(List<HasWord> keywordList : documentPreprocessor) {
            for(HasWord keyword : keywordList) {
                System.out.println(keyword.word());
            }
        }
    }

    @Test
    public void testParse() throws IOException {
        int iterations = 0;
        for(List<HasWord> keywordList : parser.tokenize(TARGET_TEXT)){
            final Tree tree = parser.parse(keywordList);
            assertEquals(5, tree.constituents().size());
            Collection<TypedDependency> dependencies = parser.dependencyParse(tree);
            assertEquals(7, IteratorUtils.toList(dependencies.iterator()).size());
            iterations++;
        }
        assertEquals(1, iterations);
    }

    @Test
    public void testPOSTag() throws IOException {
        final Tree tree = parser.parse(parser.tokenize(TARGET_TEXT).iterator().next());
        ArrayList<TaggedWord> taggedWords = parser.posTag(tree);
        assertEquals(
                "[Mortar/JJ, assault/NN, leaves/VBZ, at/IN, least/JJS, 18/CD, dead/VBN, ./.]",
                taggedWords.toString()
        );
    }

}
