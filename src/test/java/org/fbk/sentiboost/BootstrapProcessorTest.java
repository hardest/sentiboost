/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * Test case for {@link org.fbk.sentiboost.BootstrapProcessor}.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class BootstrapProcessorTest {

    @Test
    public void testDetermineSentiment() throws IOException {
        final BootstrapProcessor bootstrapProcessor = new BootstrapProcessor(loadProperties());

        Assert.assertTrue(bootstrapProcessor.determineSentiment(
                "great#great#JJ hotel#hotel#NN") > 0f
        );
        Assert.assertTrue(bootstrapProcessor.determineSentiment(
                "horrible#horrible#JJ experience#experience#NN hotel#hotel#NN") < 0f
        );
    }

    @Test
    public void testRunBootstrapping() throws IOException {
        final File outDir = ResourceUtils.getOutFile("boost/");
        outDir.mkdirs();
        BootstrapProcessor.runBootstrapping(loadProperties(), new File("./boost/parsed_reviews.txt"), outDir);
    }

    private Properties loadProperties() throws IOException {
        final Properties conf = new Properties();
        conf.load(FileUtils.openInputStream(new File("./config/default.properties")));
        return conf;
    }

}
