/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import org.annolab.tt4j.TreeTaggerException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;

/**
 * Test case for {@link org.fbk.sentiboost.POSTagger}.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class POSTaggerTest {

    private POSTagger posTagger;

    @Before
    public void setUp() {
        posTagger = new POSTagger();
    }

    @Test
    public void testTag() throws IOException, TreeTaggerException {
        final String EXPECTED =
                "mortar\tNN\tmortar\n" +
                "assault\tNN\tassault\n" +
                "leaves\tVVZ\tleave\n" +
                "at\tIN\tat\n" +
                "least\tJJS\tleast\n" +
                "18\tCD\t@card@\n" +
                "dead\tJJ\tdead\n" +
                ".\tSENT\t.\n";

        assertEquals(
                EXPECTED,
                POSTagger.serialize(posTagger.tagSentence(StanfordParserTest.TARGET_TEXT))
        );
    }

}
