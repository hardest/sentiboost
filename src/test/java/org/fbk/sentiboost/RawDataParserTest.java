/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import org.annolab.tt4j.TreeTaggerException;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

/**
 * Test case for {@link org.fbk.sentiboost.RawDataParser}.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class RawDataParserTest {

    @Test
    public void testRawParse() throws IOException, TreeTaggerException {
        final File rawDataDir = new File("./raw_data");
        final File inFile = new File(rawDataDir, "input1.txt");
        File outFile = RawDataParser.getOutFile(inFile);
        final File expectedOutFile = new File(rawDataDir, outFile.getName() + "_expected");
        RawDataParser.parseRawData(inFile, outFile);
        Assert.assertTrue(outFile.exists());
        Assert.assertEquals(
                FileUtils.readFileToString(expectedOutFile),
                FileUtils.readFileToString(outFile)
        );
    }

}
