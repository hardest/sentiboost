/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import net.didion.jwnl.JWNLException;
import org.annolab.tt4j.TreeTaggerException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Test case for {@link org.fbk.sentiboost.LexiconProcessor}.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class LexiconProcessorTest {

    @Test
    public void testRun() throws SQLException, TreeTaggerException, JWNLException, IOException {
        final File lexiconDir = new File("./lexicon");
        LexiconProcessor.processParsedReviews(
                new File(lexiconDir, "training-pos-example1.txt"),
                new File(lexiconDir, "training-neg-example1.txt"),
                null, // new File("./lexicon_db"),
                ResourceUtils.getOutFile("lexicon_out")
        );
    }

}
