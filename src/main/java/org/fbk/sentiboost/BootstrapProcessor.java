/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

/**
 * @author Gozde Ozbal (g.ozbal@trentorise.eu)
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class BootstrapProcessor {

    public static final String POSITIVE_OUT_FILE = "pos_iter_%d.txt";
    public static final String NEGATIVE_OUT_FILE = "neg_iter_%d.txt";

    private static final Logger logger = Logger.getLogger(BootstrapProcessor.class);

    private final Map<String, PolarityCount> lemmaToPosNegCount = new HashMap<>();

    private final Set<String> stopWords = ResourceUtils.loadStopWords();
    private final Set<String> negMarkers = ResourceUtils.loadNegMarkers();

    private final Map<String, Float> posWordsToProb = new HashMap<>(); //map for positive words, key: lemma, value: probability
    private final Map<String, Float> negWordsToProb = new HashMap<>(); //map for negative words, key: lemma, value: probability

    private final int minCount; //percentage from the sorted list, appears as m in the paper
    private final int nMin; //terms which occur less than nmin times are ignored, appears as nmin in the paper
    private final int numOfIterations; //number of iterations

    /**
     * args[0]: propsPath (e.g config/default.properties)
     * args[1]: inputFile (e.g. ExampleUnlabeledParsedReviews.txt)
     * args[2]: outputFolder (e.g. lexicon_bootstrapping)
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final Properties props = new Properties();
        props.load(new FileReader(args[0]));
        runBootstrapping(props, new File(args[1]), new File(args[2]));
    }

    /**
     * the input file should be already parsed with the token pos and lemma information.
     * The output of {@link org.fbk.sentiboost.RawDataParser} is compatible with the required format.
     *
     * @param config
     * @param inFile
     * @param outDir
     * @throws IOException
     */
    public static void runBootstrapping(Properties config, File inFile, File outDir) throws IOException {
        final BootstrapProcessor bootstrapper = new BootstrapProcessor(config);
        bootstrapper.bootstrap(inFile, outDir);
    }

    public BootstrapProcessor(Properties config) throws IOException {
        minCount = Integer.parseInt(config.getProperty("minCount"));
        nMin = Integer.parseInt(config.getProperty("nMin"));
        numOfIterations = Integer.parseInt(config.getProperty("numOfIter"));

        loadPosWords();
        loadNegWords();
    }

    /**
     * Determines the overall sentiment of a review.
     */
    public float determineSentiment(String review) {
        float sentOfReview = 0;
        String[] splittedReview = review.split("\\s+"); //split the review from the spaces
        float negMarkerValue = 1;
        for (String aSplittedReview : splittedReview) {
            String[] tokenLemmaPos = aSplittedReview.split("#");
            String token = tokenLemmaPos[0];
            //String lemma = tokenLemmaPos[1];
            if (!stopWords.contains(token)) {
                // The negative marker value is set to 1 for the first token of the review.
                float sentOfToken = negMarkerValue * getSentimentForToken(token);
                sentOfReview += sentOfToken;
            }
            // Check if the current token is a negative marker to be used for the next iteration.
            negMarkerValue = checkIfNegativeMarker(token);
        }
        // Take the avg by dividing the score by the length of the review so that the score is between -1 and +1.
        return sentOfReview / splittedReview.length;
    }

    /**
     * Performs bootstrapping over an <code>input file</code>.
     *
     * @param inputFile
     * @param outputDir
     * @throws IOException
     */
    public void bootstrap(File inputFile, File outputDir) throws IOException {
        if (outputDir.mkdir()) {
            logger.info("created directory: " + outputDir.getAbsolutePath());
        }

        final Map<String, Float> allReviews = readWholeFileToMap(inputFile);
        logger.info("Iterations total #: " + numOfIterations);
        for (int i = 0; i < numOfIterations; i++) { //repeat the whole process for numOfIterations times
            logger.info("iteration #: " + i);
            // Create the positive lexicon for the current iteration.
            final File posWordFile = new File(outputDir, String.format(POSITIVE_OUT_FILE, i));
            final File negWordFile = new File(outputDir, String.format(NEGATIVE_OUT_FILE, i));
            try (
                    final BufferedWriter posWordBufWriter = new BufferedWriter(new FileWriter(posWordFile));
                    final BufferedWriter negWordBufWriter = new BufferedWriter(new FileWriter(negWordFile))
            ) {
                // For each review, determine the sentiment and update the relevant value in the map.
                for (Entry<String, Float> entry : allReviews.entrySet()) {
                    final String review = entry.getKey();
                    final float sentiment = determineSentiment(review);
                    logger.debug(String.format("Review: %s - Sentiment: %f", review, sentiment));
                    entry.setValue(sentiment);
                }

                // Sort the reviews based on their sentiment values in decreasing order
                final List<Entry<String, Float>> entryVector = new ArrayList<>(allReviews.entrySet());
                Collections.sort(
                        entryVector,
                        new Comparator<Entry<String, Float>>() {
                            @Override
                            public int compare(Entry<String, Float> o1, Entry<String, Float> o2) {
                                if (o1.getValue() > o2.getValue()) {
                                    return -1;
                                } else if (o1.getValue() < o2.getValue()) {
                                    return +1;
                                }
                                return 0;
                            }
                        }
                );

                /* Extract the lemmas from the most positive and negative reviews
                   and update the maps posWordsToProb and negWordsToProb. */
                extractPosAndNegWords(entryVector);

                // Sort the positive and negative lemmas based on their probabilities.
                List<Entry<String, Float>> sortedPosWordsToProb = sortBasedOnProbs(posWordsToProb);
                List<Entry<String, Float>> sortedNegWordsToProb = sortBasedOnProbs(negWordsToProb);

                // Write the positive and negative lemmas and their probabilities to the lexicon files.
                for (Entry<String, Float> entry : sortedPosWordsToProb) {
                    posWordBufWriter.write(entry.getKey() + "\t" + entry.getValue() + "\n");
                }
                for (Entry<String, Float> entry : sortedNegWordsToProb) {
                    negWordBufWriter.write(entry.getKey() + "\t" + entry.getValue() + "\n");
                }
            }
        }
    }

    /**
     * Loads positive seeds to the relevant map.
     *
     * @throws IOException
     */
    private void loadPosWords() throws IOException {
        for (String word : ResourceUtils.loadPosWords()) {
            posWordsToProb.put(word, 0f);
        }
    }

    /**
     * Loads negative seeds to the relevant map.
     *
     * @throws IOException
     */

    private void loadNegWords() throws IOException {
        for (String word : ResourceUtils.loadNegWords()) {
            negWordsToProb.put(word, 0f);
        }
    }

    /**
     * Returns sentiment of a token. returns 1 if the token is positive, -1 if it is negative, 0 if neither.
     *
     * @param token
     * @return
     */
    private float getSentimentForToken(String token) {
        if (posWordsToProb.containsKey(token)) {
            return 1;
        } else if (negWordsToProb.containsKey(token)) {
            return -1;
        }
        return 0;
    }

    /**
     * Checks if the token is a negative marker.
     *
     * @param token
     * @return
     */
    private float checkIfNegativeMarker(String token) {
        if (negMarkers.contains(token)) {
            return -1;
        } else return 1;
    }

    /**
     * Scans the whole file and put each line to the map storing the reviews.
     *
     * @param inputFile
     * @return
     * @throws IOException
     */
    private Map<String, Float> readWholeFileToMap(File inputFile) throws IOException {
        try (final BufferedReader bufReader = new BufferedReader(new FileReader(inputFile))) {
            String inLine;
            final Map<String, Float> allReviews = new HashMap<>();
            while ((inLine = bufReader.readLine()) != null) {
                inLine = inLine.trim();
                if (inLine.length() > 0) {
                    allReviews.put(inLine, 0.f); //the sentiment score is set as 0 by default
                }
            }
            return allReviews;
        }
    }

    private List<Entry<String, Float>> sortBasedOnProbs(Map<String, Float> wordsToProb) {
        List<Entry<String, Float>> entryVector = new ArrayList<>(wordsToProb.entrySet());
        Collections.sort(entryVector, new Comparator<Entry<String, Float>>() {
            @Override
            public int compare(Entry<String, Float> o1, Entry<String, Float> o2) {
                if (o1.getValue() > o2.getValue()) {
                    return -1;
                } else if (o1.getValue() < o2.getValue()) {
                    return +1;
                }
                return 0;
            }
        });
        return entryVector;
    }

    /**
     * Extracts the lemmas from the most positive and negative reviews.
     *
     * @param entryList
     */
    private void extractPosAndNegWords(List<Entry<String, Float>> entryList) {
        final int listSize = entryList.size();
        int i = 0;
        for (Entry<String, Float> entry : entryList) {
            // Process the lemmas from the first one third of the sorted reviews with a positive sentiment.
            if (i < listSize / 3) {
                if (entry.getValue() > 0) {
                    if(logger.isDebugEnabled())
                        logger.debug(
                                String.format("+ sentence: %s sentiment score: %f", entry.getKey(), entry.getValue())
                        );
                    insertLemmasOfReviewToLemmaToPosNegCount(entry, true);
                }
            }
            // Process the lemmas from the last one third of the sorted reviews with a negative sentiment.
            else if (i >= listSize * 2 / 3) {
                if (entry.getValue() < 0) {
                    if(logger.isDebugEnabled())
                        logger.debug(
                                String.format("- sentence: %s sentiment score: %f", entry.getKey(), entry.getValue())
                        );
                    insertLemmasOfReviewToLemmaToPosNegCount(entry, false);
                }
            }
            i++;
        }

        /*
            Extend the positive lexicon.
            Sort the map lemmaToPosNegCount based on:
            (frequency in most positive reviews)
                /
            ((frequency in most positive reviews) + (frequency in most negative reviews))
        */
        List<Entry<String, PolarityCount>> lemmaToPosNegCountEntries = new ArrayList<>(lemmaToPosNegCount.entrySet());
        Collections.sort(
                lemmaToPosNegCountEntries,
                new Comparator<Entry<String, PolarityCount>>() {
                    @Override
                    public int compare(Entry<String, PolarityCount> o1, Entry<String, PolarityCount> o2) {
                        float o1Div =
                                ((float) o1.getValue().posCount) / (o1.getValue().posCount + o1.getValue().negCount);
                        float o2Div =
                                ((float) o2.getValue().posCount) / (o2.getValue().posCount + o2.getValue().negCount);
                        if (o1Div > o2Div) {
                            return -1;
                        } else if (o1Div < o2Div) {
                            return +1;
                        }
                        return 0;
                    }
                }
        );

        if (logger.isDebugEnabled()) {
            for (Entry<String, PolarityCount> entry : lemmaToPosNegCountEntries) {
                logger.debug(String.format(
                        "LEMMA: %s positive count: %d negative count: %s",
                        entry.getKey(), entry.getValue().posCount, entry.getValue().negCount
                ));
            }
        }

        // Only consider minCount percent of the sorted lemmas.
        float numOfLemmasToConsider = minCount * ((float) lemmaToPosNegCount.size()) / 100;
        for (int ind = 0; ind < numOfLemmasToConsider; ind++) {
            Entry<String, PolarityCount> entry = lemmaToPosNegCountEntries.get(ind);
            final String lemmaToAdd = entry.getKey();
            final PolarityCount polarityCount = entry.getValue();
            if(logger.isDebugEnabled()) logger.debug(String.format("Positive lemma added: %s", lemmaToAdd));
            // Lemmas which do not occur more than nmin times are ignored.
            if (polarityCount.posCount > nMin) {
                // Calculate the probability.
                float prob = ((float) polarityCount.posCount) / (polarityCount.posCount + polarityCount.negCount);
                posWordsToProb.put(lemmaToAdd, prob); // Update the posWordsToProb map with the new probability.
            }
        }

        /*
            Extend the negative lexicon.
            sort the map lemmaToPosNegCount this time based on:
                (frequency in most negative reviews)
                    /
                ((frequency in most positive reviews) + (frequency in most negative reviews)
         */
        Collections.sort(lemmaToPosNegCountEntries, new Comparator<Entry<String, PolarityCount>>() {
            @Override
            public int compare(Entry<String, PolarityCount> o1, Entry<String, PolarityCount> o2) {
                float o1Div = ((float) o1.getValue().negCount) / (o1.getValue().posCount + o1.getValue().negCount);
                float o2Div = ((float) o2.getValue().negCount) / (o2.getValue().posCount + o2.getValue().negCount);
                if (o1Div > o2Div) {
                    return -1;
                } else if (o1Div < o2Div) {
                    return +1;
                }
                return 0;
            }
        });

        if (logger.isDebugEnabled()) {
            for (Entry<String, PolarityCount> entry : lemmaToPosNegCountEntries) {
                logger.debug(String.format(
                        "LEMMA: %s positive count: %d negative count: %s",
                        entry.getKey(), entry.getValue().posCount, entry.getValue().negCount
                ));
            }
        }

        for (int ind = 0; ind < numOfLemmasToConsider; ind++) {
            Entry<String, PolarityCount> entry = lemmaToPosNegCountEntries.get(ind);
            final String lemmaToAdd = entry.getKey();
            final PolarityCount polarityCount = entry.getValue();
            if(logger.isDebugEnabled()) logger.debug(String.format("Negative lemma added: %s", lemmaToAdd));
            // Lemmas which do not occur more than nmin times are ignored.
            if (polarityCount.negCount > nMin) {
                // Calculate the probability.
                float prob = ((float) polarityCount.negCount) / (polarityCount.posCount + polarityCount.negCount);
                negWordsToProb.put(lemmaToAdd, prob); // Update the negWordsToProb map with the new probability.
            }
        }
    }

    /**
     * Stores statistics for lemmas about how many times each lemma has been observed
     * in most positive and most negative reviews.
     */
    private void updateLemmaToPosNegCount(String lemma, boolean polarity) {
        final PolarityCount curPolarityCount = lemmaToPosNegCount.get(lemma);
        if (curPolarityCount == null) { //if the lemma is not already in the map, initialize it with 1.
            if (polarity) {
                lemmaToPosNegCount.put(lemma, new PolarityCount(1, 0));
            } else {
                lemmaToPosNegCount.put(lemma, new PolarityCount(0, 1));
            }
        } else { // If the lemma is already in the map, update the relevant value.
            if (polarity) {
                curPolarityCount.posCount++;
            } else {
                curPolarityCount.negCount++;
            }
        }
    }

    /**
     * Stores the lemmas of a review to lemmaToPosNegCount to update the statistics for positive and negative counts.
     */
    private void insertLemmasOfReviewToLemmaToPosNegCount(Entry<String, Float> entry, boolean polarity) {
        final Set<String> lemmasOfOneReview = new HashSet<>();
        String review = entry.getKey();
        String[] splitReview = review.split("\\s+");
        for (String split : splitReview) {
            String[] tokenLemmaPos = split.split("#");
            String token = tokenLemmaPos[0];
            String lemma = tokenLemmaPos[1];
            // if lemma is null, put the token in the map instead of the lemma
            if (lemma.equals("null")) {
                lemma = token;
            }
            //if the lemma has already been processed in the same review before, ignore it
            if (lemmasOfOneReview.contains(lemma)) {
                continue;
            }
            lemmasOfOneReview.add(lemma); //update the set storing the lemmas in a review
            if (!stopWords.contains(token)) { //if the token does not exist in the stop word list
            }
            updateLemmaToPosNegCount(lemma, polarity);
        }
    }

    private class PolarityCount {
        int posCount = 0;
        int negCount = 0;

        PolarityCount(int newPosCount, int newNegCount) {
            posCount = newPosCount;
            negCount = newNegCount;
        }
    }

}
