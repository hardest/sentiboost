/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import java.io.File;
import java.sql.SQLException;

/**
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public interface LemmaDB {

    void openConnection(File dbPath);

    void closeConnection();

    void insertOccurrence(String lemma, String pos, boolean polarity) throws SQLException;

    void visitOccurrences(LemmaDBVisitor visitor);

}
