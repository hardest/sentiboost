/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import net.didion.jwnl.JWNLException;
import org.annolab.tt4j.TreeTaggerException;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Gozde Ozbal (g.ozbal@trentorise.eu)
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class LexiconProcessor {

    public static final String OUT_FILE_EXT = ".txt";
    public static final String POSITIVE_OUT_FILE = "pos" + OUT_FILE_EXT;
    public static final String NEGATIVE_OUT_FILE = "neg" + OUT_FILE_EXT;

    private static final Logger logger = Logger.getLogger(LexiconProcessor.class);

    private final Set<String> stopWords = ResourceUtils.loadStopWords();

    public static String convertToOneLetterPos(String tag) {
        final char firstChar = tag.charAt(0);
        switch (firstChar) {
            case 'N':
                return "n";
            case 'J':
                return "a";
            case 'V':
                return "v";
            case 'R':
                return "r";
            default:
                return null;
        }
    }

    public static void processParsedReviews(
            File posTrainingFile, File negTrainingFile, File dbPath, File lexiconOut
    ) throws IOException, JWNLException, TreeTaggerException, SQLException {
        checkExists("Cannot find POS training file", posTrainingFile);
        checkExists("Cannot find NEG training file", negTrainingFile);
        final LexiconProcessor lexiconProcessor = new LexiconProcessor();
        final LemmaDB db = dbPath == null ? new InMemoryLemmaDB() : new SQliteLemmaDB();
        try {
            db.openConnection(dbPath);
            // Collect tf/idf statistics.
            final int totalNumberOfReviews = lexiconProcessor.processParsedReviews(
                    db, posTrainingFile, negTrainingFile
            );
            // Generate sentiment lexicons.
            lexiconProcessor.createLexicons(db, totalNumberOfReviews, lexiconOut);
        } finally {
            db.closeConnection();
        }
    }

    private static void checkExists(String msg, File f) {
        if(!f.exists()) throw new IllegalStateException(String.format("%s [%s]", msg, f.getAbsolutePath()));
    }

    /**
     * 1st arg: pos parsed training file
     * 2nd arg: neg parsed training file
     * 3rd arg: path_SQlite = "db/counts";
     * 4th arg: folder for the lexicons that will be generated
     */
    public static void main(String[] args) throws IOException, JWNLException, TreeTaggerException, SQLException {
        final File posTrainingFile = new File(args[0]);
        final File negTrainingFile = new File(args[1]);
        final File dbPath = args[2].trim().length() == 0 ? null : new File(args[2]);
        final File lexiconOut = new File(args[3]);
        processParsedReviews(posTrainingFile, negTrainingFile, dbPath, lexiconOut);
    }

    public LexiconProcessor() throws IOException, JWNLException {
    }

    public int processParsedReviews(LemmaDB db, File posTrainingFile, File negTrainingFile)
            throws IOException, TreeTaggerException, SQLException {
        final int numOfPosTrainingReviews = processParsedReviews(db, posTrainingFile, true);
        final int numOfNegTrainingReviews = processParsedReviews(db, negTrainingFile, false);
        int totalNumberOfReviews = numOfPosTrainingReviews + numOfNegTrainingReviews;
        logger.info(String.format(
                "Positive reviews training: %d, Negative reviews training: %d, Total reviews training: %d",
                numOfPosTrainingReviews,
                numOfNegTrainingReviews,
                totalNumberOfReviews
        ));
        return totalNumberOfReviews;
    }

    public int processParsedReviews(LemmaDB dbOps, File file, boolean polarity)
            throws IOException, TreeTaggerException, SQLException {
        try (final BufferedReader corpusReader = new BufferedReader(new FileReader(file))) {
            String line;
            int processedReviews = 0;
            HashSet<String> sameDocumentLemmas = new HashSet<>(); //Set of lemmas existing in the same review.
            while ((line = corpusReader.readLine()) != null) {
                line = line.trim();
                if (line.length() == 0) continue;
                if(logger.isDebugEnabled())
                    logger.debug(String.format("Processing line [%s] with polarity %s\n" , line, polarity));
                processedReviews++;
                sameDocumentLemmas.clear();
                // Process phrase.
                String[] splitContentText = line.split("\\s+");
                for (String tokenPosLemma : splitContentText) {
                    // Process annotation.
                    String[] tokenPosLemmaArr = tokenPosLemma.split("#");
                    String token = tokenPosLemmaArr[0];
                    String lemma = tokenPosLemmaArr[1];
                    String pos = tokenPosLemmaArr[2];
                    /* Only consider the triple if the token consists of alphabetic characters,
                      lemma is neither null nor a stop word. */
                    if (token.matches("[a-zA-Z]*") && !lemma.equals("null") && !stopWords.contains(lemma)) {
                        String oneLetterPos = convertToOneLetterPos(pos);
                        if (oneLetterPos != null) { // If POS is foreseen.
                            final String lemmaID = lemma + "#" + oneLetterPos;
                            if (sameDocumentLemmas.contains(lemmaID)) { // The lemma pos pair is already in the set.
                            } else { // The lemma pos pair is not in the set, add it.
                                sameDocumentLemmas.add(lemmaID);
                            }
                            // Insert the statistics into the database.
                            dbOps.insertOccurrence(lemma, oneLetterPos, polarity);

                        }
                    }
                }

            }
            return processedReviews;
        }
    }

    //creates positive and negative sentiment lexicons under the input folder
    public void createLexicons(LemmaDB db, final int totalNumberOfReviews, File outDir)
            throws IOException, SQLException {
        // If the directory does not exist, create it.
        boolean result = outDir.mkdirs();
        if (result) {
            logger.info(String.format("Out dir [%s] created.", outDir.getAbsolutePath()));
        }

        try (
                final BufferedWriter posWriter = new BufferedWriter(
                        new FileWriter(new File(outDir, POSITIVE_OUT_FILE))
                );
                final BufferedWriter negWriter = new BufferedWriter(
                        new FileWriter(new File(outDir, NEGATIVE_OUT_FILE))
                )
        ) {
            db.visitOccurrences(new LemmaDBVisitor() {
                @Override
                public void entry(String lemma, String pos, int tfPos, int tfNeg, int idf) {
                    double tfIdfPos = Math.log(tfPos + 1) * Math.log(totalNumberOfReviews / idf);
                    double tfIdfNeg = Math.log(tfNeg + 1) * Math.log(totalNumberOfReviews / idf);
                    double deltaTfIdf = tfIdfPos - tfIdfNeg;
                    try {
                        if (deltaTfIdf > 0) {
                            posWriter.write(lemma + "#" + pos + "\t" + deltaTfIdf + "\n");
                        } else if (deltaTfIdf < 0) {
                            negWriter.write(lemma + "#" + pos + "\t" + deltaTfIdf + "\n");
                        }
                    } catch (IOException ioe) {
                        throw new RuntimeException("Error while writing results.", ioe);
                    }
                }
            });
        }
    }

}
