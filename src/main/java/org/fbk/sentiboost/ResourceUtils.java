/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Gozde Ozbal (g.ozbal@trentorise.eu)
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class ResourceUtils {

    public static final File OUT_DIR = new File("./out");

    public static final String STOPWORDS_LIST = "/lexical/stopwords.txt";
    private static final String POSWORDS_LIST = "/lexical/positive_words.txt";
    private static final String NEGWORDS_LIST = "/lexical/negative_words.txt";
    private static final String NEGMARKERS_LIST = "/lexical/negative_markers.txt";

    public static Set<String> loadStopWords() throws IOException {
        return loadResource(STOPWORDS_LIST);
    }

    public static Set<String> loadPosWords() throws IOException {
        return loadResource(POSWORDS_LIST);
    }

    public static Set<String> loadNegWords() throws IOException {
        return loadResource(NEGWORDS_LIST);
    }

    public static Set<String> loadNegMarkers() throws IOException {
        return loadResource(NEGMARKERS_LIST);
    }

    public static File getOutFile(String fileName) {
        return new File(OUT_DIR, fileName);
    }

    private static Set<String> loadResource(String res) throws IOException {
        return new HashSet(IOUtils.readLines(ResourceUtils.class.getResourceAsStream(res)));
    }

}
