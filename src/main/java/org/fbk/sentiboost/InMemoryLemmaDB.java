/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import java.io.File;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class InMemoryLemmaDB implements LemmaDB {

    private Map<String, LemmaStat> lemmaStats = new HashMap();


    @Override
    public void openConnection(File dbPath) {
        // Empty.
    }

    @Override
    public void closeConnection() {
        lemmaStats.clear();
    }

    @Override
    public void insertOccurrence(String lemma, String pos, boolean polarity) throws SQLException {
        final String key = String.format("%s#%s", lemma, pos);
        LemmaStat lemmaCounters = lemmaStats.get(key);
        if(lemmaCounters == null) {
            lemmaCounters = new LemmaStat(lemma, pos, polarity);
            lemmaStats.put(key, lemmaCounters);
        } else {
            lemmaCounters.update(polarity);
        }
    }

    @Override
    public void visitOccurrences(LemmaDBVisitor visitor) {
        for(LemmaStat lemmaCounters : lemmaStats.values()) {
            visitor.entry(
                    lemmaCounters.lemma, lemmaCounters.pos,
                    lemmaCounters.tfPos, lemmaCounters.tfNeg, lemmaCounters.idf
            );
        }
    }

    private class LemmaStat {

        private final String lemma;
        private final String pos;
        private int tfPos = 0;
        private int tfNeg = 0;
        private int idf = 0;

        public LemmaStat(String lemma, String pos, boolean polarity) {
            this.lemma = lemma;
            this.pos = pos;
            update(polarity);
        }

        public void update(boolean polarity) {
            if (polarity) {
                tfPos++;
            } else {
                tfNeg++;
            }
            idf++;
        }
    }
}
