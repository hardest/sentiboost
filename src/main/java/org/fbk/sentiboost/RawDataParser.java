/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import net.didion.jwnl.JWNLException;
import org.annolab.tt4j.TreeTaggerException;
import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collection;

/**
 * @author Gozde Ozbal (g.ozbal@trentorise.eu)
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class RawDataParser {

    private final POSTagger posTagger;

    public static File getOutFile(File inFile) {
        return new File(ResourceUtils.getOutFile("raw_data"), inFile.getName() + "_parsed");
    }

    public static void parseRawData(File inFile, File outFile) throws IOException, TreeTaggerException {
        final RawDataParser parser = new RawDataParser();
        try (
                final BufferedReader in = new BufferedReader(
                        new InputStreamReader(FileUtils.openInputStream(inFile))
                );
                final Writer out = new BufferedWriter(
                        new OutputStreamWriter(FileUtils.openOutputStream(outFile))
                )
        ) {
            parser.parseRawData(in, out);
        }
    }

    public static void main(String[] args) throws IOException, JWNLException, TreeTaggerException {
        final File inFile = new File(args[0]);
        parseRawData(inFile, getOutFile(inFile));
    }

    public RawDataParser() {
        posTagger = new POSTagger();
    }

    public void parseRawData(BufferedReader in, Writer out) throws IOException, TreeTaggerException {
        String line;
        StringBuilder outputBuffer = new StringBuilder();
        while ((line = in.readLine()) != null) {
            line = line.trim();
            if (line.length() == 0) continue;
            Collection<Annotation> annotations = posTagger.tagSentence(line);
            for (Annotation annotation : annotations) {
                outputBuffer.append(annotation.getToken());
                outputBuffer.append('#');
                outputBuffer.append(annotation.getLemma());
                outputBuffer.append('#');
                outputBuffer.append(annotation.getPos());
                outputBuffer.append(' ');
            }
            outputBuffer.append('\n');
            out.write(outputBuffer.toString());
            outputBuffer.delete(0, outputBuffer.length());
        }

    }

}
