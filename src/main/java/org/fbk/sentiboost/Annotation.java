/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

/**
 * A {@link org.fbk.sentiboost.POSTagger} word annotation.
 *
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class Annotation {

    private final String token;
    private final String pos;
    private final String lemma;

    public Annotation(String token, String pos, String lemma) {
        this.token = token;
        this.pos = pos;
        this.lemma = lemma;
    }

    public String getToken() {
        return token;
    }

    public String getPos() {
        return pos;
    }

    public String getLemma() {
        return lemma;
    }

    public void serialize(StringBuilder sb) {
        sb.append(token).append('\t')
        .append(pos).append('\t')
        .append(lemma);
    }

}
