/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * @author Gozde Ozbal (g.ozbal@trentorise.eu)
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class StanfordParser {

    private static final String ENGLISH_GRAMMAR_RESOURCE = "/parsing/englishPCFG.ser.gz";

    private TreebankLanguagePack tlp;
    private GrammaticalStructureFactory gsf;
    private LexicalizedParser lp;

    public static LexicalizedParser loadParserFromResource() {
        try {
            ObjectInputStream modelInputStream = new ObjectInputStream(
                    new GZIPInputStream(StanfordParser.class.getResourceAsStream(ENGLISH_GRAMMAR_RESOURCE))
            );
            return LexicalizedParser.loadModel(modelInputStream);
        } catch (Exception ex) {
            throw new IllegalArgumentException(
                    "Failed to load serialized parsed from resource: " + ENGLISH_GRAMMAR_RESOURCE, ex
            );
        }
    }

    public StanfordParser() {
        lp = loadParserFromResource();
        tlp = new PennTreebankLanguagePack();
        gsf = tlp.grammaticalStructureFactory();
        lp.setOptionFlags("-maxLength", "80", "-retainTmpSubcategories");
    }

    public DocumentPreprocessor tokenize(String paragraph) throws IOException {
        Reader reader = new StringReader(paragraph);
        return new DocumentPreprocessor(reader);
    }

    public Tree parse(List<? extends HasWord> sentence) {
        return lp.apply(sentence);
    }

    public Collection<TypedDependency> dependencyParse(Tree parse) {
        GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
        return gs.typedDependencies();
    }

    public Collection<TypedDependency> dependencyParseCoreLabelList(List<CoreLabel> sentence) {
        Tree parse = lp.apply(sentence);
        GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
        return gs.typedDependencies();
    }

    public ArrayList<TaggedWord> posTag(Tree parse) {
        return parse.taggedYield();
    }

}
