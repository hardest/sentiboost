/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class SQliteLemmaDB implements LemmaDB {

    private Connection conn;

    @Override
    public void openConnection(File dbPath) {
        if (conn != null) {
            closeConnection();
        }
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath.getAbsolutePath());
            Statement stm = conn.createStatement();
            stm.execute("PRAGMA cache_size=500000");
            stm.execute("PRAGMA synchronous=OFF");
            stm.execute("PRAGMA count_changes=OFF");
            stm.execute("PRAGMA temp_store=MEMORY");
            stm.close();

            //create the counts table if it already does not exist
            try (PreparedStatement ps = conn.prepareStatement(
                    "create table if not exists counts " +
                            "(lemma varchar(1000), pos varchar(5), tfPos Integer, tfNeg Integer, idf Integer, " +
                            "primary key(lemma,pos));"
            )) {
                ps.execute();
            }
            ;
        } catch (Exception e) {
            throw new RuntimeException("Error while opening connection.", e);
        }
    }

    @Override
    public void closeConnection() {
        try {
            conn.close();
            conn = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void insertOccurrence(String lemma, String pos, boolean polarity)
            throws SQLException {
        final PreparedStatement ps = conn.prepareStatement(
                "select lemma, pos, tfPos, tfNeg, idf from counts where lemma=? and pos=?;"
        );
        ps.setString(1, lemma);
        ps.setString(2, pos);
        try (final ResultSet rs = ps.executeQuery()) {
            // Lemma does not exist in the table, first time addition.
            if (!rs.next()) {
                final PreparedStatement psInsert = conn.prepareStatement(
                        "insert into counts (lemma, pos, tfPos, tfNeg, idf) values (?,?,?,?,?)"
                );
                psInsert.setString(1, lemma);
                psInsert.setString(2, pos);
                if (polarity) { //if it is coming from a positive review, set tfPos to 1 and tfNeg to 0.
                    psInsert.setInt(3, 1);
                    psInsert.setInt(4, 0);
                } else { //if it is coming from a negative review, set tfPos to 0 and tfNeg to 1.
                    psInsert.setInt(3, 0);
                    psInsert.setInt(4, 1);
                }
                psInsert.setInt(5, 1); //in both cases, set inverse document frequency to 1.
                psInsert.executeUpdate();
                psInsert.close();
            // Lemma already exists in the table, just do an update.
            } else {
                int curTfPos = rs.getInt("tfPos");
                int curTfNeg = rs.getInt("tfNeg");
                int curIdf   = rs.getInt("idf");
                String updateStmStr = "update counts set ";
                if (polarity) { //if it is coming from a positive review increase tfPos by 1
                    updateStmStr += "tfPos = " + (curTfPos + 1) + " ";
                } else { //if it is coming from a negative review increase tfNeg by 1
                    updateStmStr += "tfNeg = " + (curTfNeg + 1) + " ";
                }
                updateStmStr += ", idf = " + (curIdf + 1) + " ";
                updateStmStr += " where lemma = \"" + lemma + "\" and pos = \"" + pos + "\"";
                try(final Statement updateStm = conn.createStatement()) {
                    updateStm.executeUpdate(updateStmStr);
                }
            }

        }
        ps.close();
    }

    @Override
    public void visitOccurrences(LemmaDBVisitor visitor) {
        try {
            PreparedStatement ps = conn.prepareStatement("select lemma, pos, tfPos, tfNeg, idf from counts");
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String lemma = rs.getString("lemma");
                    String pos = rs.getString("pos");
                    int tfPos = rs.getInt("tfPos");
                    int tfNeg = rs.getInt("tfNeg");
                    int idf = rs.getInt("idf");
                    visitor.entry(lemma, pos, tfPos, tfNeg, idf);
                }
            }
        } catch (SQLException sqle) {
             throw new RuntimeException("Error while visiting iterating DB occurrences.", sqle);
        }
    }
}
