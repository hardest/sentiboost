/*
 *   Copyright 2014 FBK - HLT
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
package org.fbk.sentiboost;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import org.annolab.tt4j.ExecutableResolver;
import org.annolab.tt4j.PlatformDetector;
import org.annolab.tt4j.TokenHandler;
import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Gozde Ozbal (g.ozbal@trentorise.eu)
 * @author Michele Mostarda (mostarda@fbk.eu)
 */
public class POSTagger {

    private static final String TREE_TAGGER_ROOT = "TreeTagger";
    private static final String TREE_TAGGER_BIN = TREE_TAGGER_ROOT + "/bin/tree-tagger";
    private static final String TREE_TAGGER_MODEL = TREE_TAGGER_ROOT + "/lib/english.par:iso8859-1";

    private TreebankLanguagePack stanfordParser;

    public static String serialize(Collection<Annotation> annotations) {
        final StringBuilder sb = new StringBuilder();
        for(Annotation annotation : annotations) {
            annotation.serialize(sb);
            sb.append('\n');
        }
        return sb.toString();
    }

    public POSTagger() {
        stanfordParser = new PennTreebankLanguagePack();
    }

    public Collection<Annotation> tagTokens(String[] tokenArray) throws TreeTaggerException {
        //TODO: System.setProperty("treetagger.home", "/opt/treetagger");
        final TreeTaggerWrapper tt = new TreeTaggerWrapper<String>();
        try {
            try {
                tt.setModel(TREE_TAGGER_MODEL);
            } catch (IOException ioe) {
                throw new IllegalStateException("Error while configuring model", ioe);
            }

            final List<Annotation> annotations = new ArrayList<>();
            tt.setExecutableProvider(new ExecutableResolver() {
                public void setPlatformDetector(PlatformDetector platformDetector) {
                }

                public String getExecutable() throws IOException {
                    return TREE_TAGGER_BIN;
                }

                public void destroy() {
                }
            });

            tt.setHandler(new TokenHandler<String>() {
                public void token(String token, String pos, String lemma) {
                    annotations.add(new Annotation(token.toLowerCase(), pos, lemma.toLowerCase()));
                }
            });
            try {
                tt.process(Arrays.asList(tokenArray));
                return annotations;
            } catch (IOException ioe) {
                throw new IllegalStateException("Error while processing model.", ioe);
            }
        } finally {
            tt.destroy();
        }
    }

    public Collection<Annotation> tagSentence(List<HasWord> tokenizedSentence) throws TreeTaggerException, IOException {
        String[] toString = new String[tokenizedSentence.size()];
        for (int i = 0; i < toString.length; i++) {
            toString[i] = tokenizedSentence.get(i).word();
        }
        return tagTokens(toString);
    }

    public Collection<Annotation> tagSentence(String sentence) throws TreeTaggerException, IOException {
        final Tokenizer<? extends HasWord> toke = stanfordParser.getTokenizerFactory().getTokenizer(
                new StringReader(sentence)
        );
        return tagSentence((List<HasWord>) toke.tokenize());
    }

}
